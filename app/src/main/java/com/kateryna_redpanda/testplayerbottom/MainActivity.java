package com.kateryna_redpanda.testplayerbottom;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

import java.util.ArrayList;

@EActivity(R.layout.activity_main)
public class MainActivity extends AppCompatActivity {

    @ViewById(R.id.listText)
    ListView mockList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @AfterViews
    public void setupList() {
        if(mockList != null){
            ArrayList<String> mockItems = new ArrayList<>();
            for(int i = 0; i<20; i++){
                mockItems.add("One more mock item. It can be a song title, for example");
            }
            ArrayAdapter<String> mockAdapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, mockItems);
            mockList.setAdapter(mockAdapter);
        }
    }

    @Click({R.id.clickableNext, R.id.clickablePrev, R.id.fab})
    public void clickNavButton(View view){
        Toast.makeText(this, "You've clicked a button!", Toast.LENGTH_SHORT).show();
    }

}
